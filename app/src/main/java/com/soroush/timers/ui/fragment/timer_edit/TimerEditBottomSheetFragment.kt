package com.soroush.timers.ui.fragment.timer_edit

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.soroush.timers.R
import com.soroush.timers.data.Constants.TIMER_EXTRA_KEY
import com.soroush.timers.data.Timer
import com.soroush.timers.helper.DurationFormatter
import kotlinx.android.synthetic.main.fragment_timer_edit.view.*

class TimerEditBottomSheetFragment : BottomSheetDialogFragment() {

    private var listener: TimerEditListener? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is TimerEditListener)
            listener = context
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.fragment_timer_edit, container, false)

        // initialize views based on arguments
        arguments?.getParcelable<Timer>(TIMER_EXTRA_KEY)?.let { setupViews(rootView, it) }

        return rootView
    }

    private fun setupViews(rootView: View, timer: Timer) = rootView.apply {
        editText_counterTitle.setText(timer.title)
        chronometer.apply {
            base = timer.getBaseInElapsedTime()
            if (timer.isRunning) start()
            durationFormatter = DurationFormatter(singleLine = true)
        }
        buttonSave.setOnClickListener {
            timer.title = editText_counterTitle.text.toString()
            listener?.onTimerUpdated(timer)
        }
    }

    companion object {
        private val TAG = TimerEditBottomSheetFragment::class.java.canonicalName
        fun show(fm: FragmentManager, timer: Timer) {
            val fragment = TimerEditBottomSheetFragment()
            fragment.arguments = Bundle().apply {
                putParcelable(TIMER_EXTRA_KEY, timer)
            }
            fragment.show(fm, TAG)
        }
    }
}