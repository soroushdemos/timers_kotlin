package com.soroush.timers.ui.fragment.timer_edit

import com.soroush.timers.data.Timer

interface TimerEditListener {
    fun onTimerUpdated(timer: Timer)
}