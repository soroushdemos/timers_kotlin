package com.soroush.timers.ui.components.timerGrid

import com.soroush.timers.data.Timer

interface TimerGridCallbacks {
    fun onTimerDeleted(timer: Timer)
    fun showTimerEdit(timer: Timer)
    fun resetTimer(timer: Timer)
}