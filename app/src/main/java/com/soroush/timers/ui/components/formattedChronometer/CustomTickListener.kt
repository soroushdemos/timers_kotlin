package com.soroush.timers.ui.components.formattedChronometer

import android.widget.Chronometer
import com.soroush.timers.helper.DurationFormatter


class CustomTickListener constructor(
    private val durationFormatter: DurationFormatter = DurationFormatter()
) : Chronometer.OnChronometerTickListener {
    override fun onChronometerTick(chronometer: Chronometer?) {
        chronometer?.let {
            it.format = durationFormatter.formatFromBase(it.base)
        }
    }
}
