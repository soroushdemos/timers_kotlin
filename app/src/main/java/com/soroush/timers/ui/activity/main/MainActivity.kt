package com.soroush.timers.ui.activity.main

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import com.soroush.timers.R
import com.soroush.timers.data.Storage
import com.soroush.timers.data.Timer
import com.soroush.timers.data.TimersRepo
import com.soroush.timers.ui.components.timerGrid.TimerGridCallbacks
import com.soroush.timers.ui.components.timerGrid.TimerRecyclerViewAdapter
import com.soroush.timers.ui.fragment.timer_edit.TimerEditBottomSheetFragment
import com.soroush.timers.ui.fragment.timer_edit.TimerEditListener
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*

class MainActivity : AppCompatActivity(), MainView, TimerEditListener, TimerGridCallbacks {
    private lateinit var presenter: MainPresenter
    private var data = emptyList<Timer>()
    private val timerRecyclerViewAdapter = TimerRecyclerViewAdapter(data, this)

    /*****************************
     * AppCompatActivity Methods
     *****************************/
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        presenter = MainPresenter(this, TimersRepo(Storage(this)))

        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        setupRecyclerView()
        setupAddButtonListener()
    }

    override fun onResume() {
        super.onResume()
        presenter.loadTimers()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean =
        when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }

    /*****************************
     * TimerEditListener Methods
     *****************************/
    override fun onTimerUpdated(timer: Timer) = presenter.updateTimer(timer)
    override fun showTimerEdit(timer: Timer) = navigateToEditTimer(timer)
    override fun resetTimer(timer: Timer) = presenter.resetTimer(timer)

    /********************************
     * TimerDeleteListenert Methods
     ********************************/
    override fun onTimerDeleted(timer: Timer) = presenter.deleteTimer(timer)

    /*******************
     * MainView Methods
     *******************/
    override fun refreshTimers(timers: List<Timer>) {
        timerRecyclerViewAdapter.apply {
            setItems(timers)
        }
    }

    override fun navigateToEditTimer(timer: Timer) =
        TimerEditBottomSheetFragment.show(supportFragmentManager, timer)

    /*****************
     * Helper Methods
     *****************/
    private fun setupRecyclerView() = recyclerView_timers.apply {
        setHasFixedSize(true)
        layoutManager = GridLayoutManager(context, 3)
        adapter = timerRecyclerViewAdapter
    }

    private fun setupAddButtonListener() {
        fab.setOnClickListener {
            presenter.addTimer()
        }
    }
}
