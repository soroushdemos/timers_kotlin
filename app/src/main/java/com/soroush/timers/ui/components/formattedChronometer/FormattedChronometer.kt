package com.soroush.timers.ui.components.formattedChronometer

import android.content.Context
import android.util.AttributeSet
import android.widget.Chronometer
import com.soroush.timers.helper.DurationFormatter

class FormattedChronometer: Chronometer {
    constructor(context: Context): this(context, null)
    constructor(context: Context, attrs: AttributeSet?): this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    var durationFormatter: DurationFormatter
        set(value) {
            field = value
            onChronometerTickListener =
                CustomTickListener(
                    durationFormatter
                )
            format = durationFormatter.formatFromBase(base)
        }

    init {
        durationFormatter = DurationFormatter()
    }
}