package com.soroush.timers.ui.activity.main

import com.soroush.timers.data.Timer

interface MainView {
    fun refreshTimers(timers: List<Timer>)
    fun navigateToEditTimer(timer: Timer)
}