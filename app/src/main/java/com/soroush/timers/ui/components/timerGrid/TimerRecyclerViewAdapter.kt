package com.soroush.timers.ui.components.timerGrid

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.soroush.timers.R
import com.soroush.timers.data.Timer
import kotlinx.android.synthetic.main.viewholder_timer.view.*

class TimerRecyclerViewAdapter(
    private var items: List<Timer>,
    private var callbackListener: TimerGridCallbacks
) :
    RecyclerView.Adapter<TimerViewHolder>() {

    private var deleteList: MutableList<Timer> = mutableListOf()

    fun setItems(data: List<Timer>) {
        items = data
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TimerViewHolder =
        TimerViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.viewholder_timer, parent, false) as CardView
        )

    override fun getItemCount(): Int = items.count()

    override fun onBindViewHolder(vh: TimerViewHolder, position: Int) {
        val item = items[position]
        vh.itemView.apply {
            cardView_timer.apply {
                setOnClickListener { respondToCardTap(item) }
                setOnLongClickListener { respondToCardHold(item) }
            }
            textView_title.text = item.title
            chronometer.apply {
                if (item.isRunning) start()
                base = item.getBaseInElapsedTime()
            }
            imageButton_delete.apply {
                visibility = if (deleteList.contains(item)) View.VISIBLE else View.INVISIBLE
                setOnClickListener {
                    deleteList.remove(item)
                    callbackListener.onTimerDeleted(item)
                }
            }
            imagebutton_refresh.setOnClickListener {
                callbackListener.resetTimer(item)
            }
        }
    }

    /******************
     * Helper Methods
     ******************/
    private fun respondToCardTap(item: Timer) {
        if (deleteList.contains(item))
            deleteList.remove(item)
        else
            callbackListener.showTimerEdit(item)

        notifyDataSetChanged()
    }

    private fun respondToCardHold(item: Timer): Boolean {
        if (deleteList.contains(item))
            deleteList.remove(item)
        else
            deleteList.add(item)

        notifyDataSetChanged()

        return true
    }
}
