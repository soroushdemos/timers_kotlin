package com.soroush.timers.ui.activity.main

import com.soroush.timers.data.Timer
import com.soroush.timers.data.TimersRepo

class MainPresenter(
    private val view: MainView,
    private val timersRepo: TimersRepo
) {
    private var timers = mutableListOf<Timer>()

    fun loadTimers() {
        timers = timersRepo.getUserTimers()
        refreshTimersView()
    }

    fun addTimer() {
        timers.add(Timer())
        view.navigateToEditTimer(timers.last())
        refreshTimersView()
        saveTimers()
    }

    fun updateTimer(timer: Timer) {
        timers[findTimerIndex(timer)] = timer
        refreshTimersView()
        saveTimers()
    }

    fun deleteTimer(timer: Timer) {
        timers.removeAt(findTimerIndex(timer))
        refreshTimersView()
        saveTimers()
    }

    fun resetTimer(timer: Timer) {
        timers[findTimerIndex(timer)].reset()
        refreshTimersView()
        saveTimers()
    }

    /******************
     * Helper Methods
     ******************/
    private fun saveTimers() = timersRepo.saveUserTimers(timers)

    private fun refreshTimersView() = view.refreshTimers(timers)

    private fun findTimerIndex(timer: Timer) =
        timers.indexOf(timers.find { it.id == timer.id })
}