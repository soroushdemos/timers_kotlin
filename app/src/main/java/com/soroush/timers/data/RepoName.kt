package com.soroush.timers.data

enum class RepoName(val string: String) {
    Timers("Timers")
}