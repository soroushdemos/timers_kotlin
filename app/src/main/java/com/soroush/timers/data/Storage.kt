package com.soroush.timers.data

import android.content.Context
import java.io.File
import java.io.Serializable


class Storage(context: Context) {
    private val dir = context.filesDir

    fun loadObject(objectName: String): String =
        File(getFileName(objectName)).readText()

    fun saveObject(objectName: String, obj: Serializable) {
        val file = File(getFileName(objectName))
        file.createNewFile()
        file.writeText(obj.toString())
    }

    private fun getFileName(objectName: String) = "$dir/$objectName.json"
}