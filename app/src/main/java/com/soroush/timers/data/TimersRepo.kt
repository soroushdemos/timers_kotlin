package com.soroush.timers.data

import com.fasterxml.jackson.module.kotlin.readValue
import com.google.gson.Gson

class TimersRepo(storage: Storage) : BaseRepo(storage, RepoName.Timers) {
    fun getUserTimers(): MutableList<Timer> =
        try {
            mapper.readValue(loadObject())
        } catch (e: Throwable) {
            mutableListOf()
        }

    fun saveUserTimers(timers: List<Timer>) =
        saveObject(Gson().toJson(timers))

}
