package com.soroush.timers.data

import android.os.Parcelable
import android.os.SystemClock
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.io.Serializable
import java.util.*

@Parcelize
data class Timer(
    @SerializedName("id") val id: String = UUID.randomUUID().toString(),
    @SerializedName("title") var title: String = "",
    @SerializedName("startTime") var startTime: Long = getNow(),
    @SerializedName("isRunning") var isRunning: Boolean = true
) : Parcelable, Serializable {
    fun reset() {
        this.startTime = getNow()
    }

    fun getElapsed(): Long = System.currentTimeMillis() - startTime

    fun getBaseInElapsedTime(): Long = SystemClock.elapsedRealtime() - System.currentTimeMillis() + startTime

    companion object {
        fun getNow() = System.currentTimeMillis()
        fun durationFromBase(base: Long): Long = SystemClock.elapsedRealtime() - base
    }
}