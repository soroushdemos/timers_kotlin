package com.soroush.timers.data

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import java.io.Serializable

abstract class BaseRepo(private val storage: Storage, private val objectName: RepoName) {
    internal val mapper = ObjectMapper().registerModule(KotlinModule())
    fun loadObject(): String = storage.loadObject(objectName.string)
    fun saveObject(obj: Serializable) = storage.saveObject(objectName.string, obj)
}