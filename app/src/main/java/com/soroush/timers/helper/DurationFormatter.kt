package com.soroush.timers.helper

import com.soroush.timers.data.Timer

class DurationFormatter constructor(private val singleLine: Boolean = false) {
    fun formatFromBase(base: Long): String =
        format(Timer.durationFromBase(base))

    /**********
     * Helpers
     **********/
    private fun format(duration: Long): String {
        val weeks = duration / WEEK
        val days = (duration - weeks * WEEK) / DAY
        val hours = (duration - weeks * WEEK - days * DAY) / HOUR
        val minutes = (duration - weeks * WEEK - days * DAY - hours * HOUR) / MINUTE
        val seconds =
            (duration - weeks * WEEK - days * DAY - hours * HOUR - minutes * MINUTE) / SECOND

        val separator = if (this.singleLine) " " else "\n"
        var outString = ""
        if (weeks > 0) outString += "$weeks Weeks$separator"
        if (days > 0) outString += "$days Days$separator"
        if (hours > 0) outString += "$hours Hours$separator"
        if (minutes > 0) outString += "$minutes Minutes$separator"
        if (seconds >= 0) outString += "$seconds Seconds$separator"

        return outString
    }

    /**********
     * Static
     **********/
    companion object {
        private const val SECOND: Long = 1000
        private const val MINUTE: Long = 60 * SECOND
        private const val HOUR: Long = 60 * MINUTE
        private const val DAY: Long = 24 * HOUR
        private const val WEEK: Long = 7 * DAY
    }
}